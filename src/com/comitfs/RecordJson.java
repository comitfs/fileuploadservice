package com.comitfs;

import java.io.Serializable;

public class RecordJson implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fileLocation;

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public RecordJson(String fileLocation) {
		super();
		this.fileLocation = fileLocation;
	}
	
	
}
