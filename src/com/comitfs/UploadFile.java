package com.comitfs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.DecimalFormat;
import java.util.Properties;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UploadFile {
	private static String importURl;
	private static String directory;
	private static String sleepTime;
	private static int ignoreFileSize;
	private static DecimalFormat df = new DecimalFormat("0");
	private static String targeturl;

	public static void main(String[] args) {
		readProperties(args[0]);
		File dir = new File(directory);
		watchDirectoryPath(dir.toPath());
	}

	public static void watchDirectoryPath(Path path) {
		try {
			System.out.println(path.getFileName().toString());
			WatchService watchService
			= FileSystems.getDefault().newWatchService();


			path.register(
					watchService, 
					StandardWatchEventKinds.ENTRY_CREATE);

			WatchKey key;
			while ((key = watchService.take()) != null) {
				for (WatchEvent<?> event : key.pollEvents()) {
					String fileLocation = directory+"\\"+event.context();
					//					if(!sleepTime.isEmpty()) {
					//						Thread.sleep(Integer.parseInt(sleepTime));
					//					}
					File file = new File(fileLocation);
					Boolean isFile = isFileLocked(file);
					while(!isFile) {
						isFile = isFileLocked(file);
					}
					if(!sleepTime.isEmpty()) {
						Thread.sleep(Integer.parseInt(sleepTime));
					}
					Boolean res =invokeUploadFile(fileLocation);
					if(res) {
						deleteFile(fileLocation);
						RecordJson recordJson = new RecordJson(file.getName());
						Gson gsonBuilder = new GsonBuilder().create();
						String jsonFromPojo = gsonBuilder.toJson(recordJson);
						sendResponse(jsonFromPojo);
					}else {
						System.out.println("upload failed for the file "+ fileLocation);
					}
				}
				key.reset();
			}
		}catch(Throwable e) {
			System.err.println(e);
		}

	}


	public static boolean isNumeric(String strNum) {
		Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
		if (strNum == null) {
			return false; 
		}
		return pattern.matcher(strNum).matches();
	}

	public static Boolean invokeUploadFile(String filePath) {
		Boolean result = Boolean.FALSE;
		String charset = "UTF-8";
		double fileSize = printFileSize(filePath);
		if(fileSize < ignoreFileSize) {
			System.out.println("file size is less than "+fileSize+" ignore file "+ filePath);
			return result;
		}
		try {
			MultipartUtility multipart = new MultipartUtility(importURl, charset);
			System.out.println(filePath);

			multipart.addFilePart("fileUpload", new File(filePath));
			//multipart.addFilePart(filePath);
			int response = multipart.finish();

			System.out.println("SERVER REPLIED:" + response);

			if(response == HttpURLConnection.HTTP_OK) {
				result = Boolean.TRUE;
			}
		} catch(Exception e) {
			System.err.println(e);
		}
		return result;
	}

	public static Boolean isFileLocked(File file)
	{
		Boolean result = Boolean.FALSE;
		result = file.renameTo(file);
		return result;
	}

	public static void deleteFile(String filePath) {

		File file = new File(filePath); 

		if(file.delete()) 
		{ 
			System.out.println("File deleted successfully " + filePath); 
		} 
		else
		{ 
			System.out.println("Failed to delete the file "+ filePath); 
		} 
	}

	public static void readProperties(String fileName) {
		try {

			InputStream input = new FileInputStream(fileName);
			Properties prop = new Properties();
			prop.load(input);
			directory = prop.getProperty("directory");
			importURl = prop.getProperty("importurl");
			sleepTime = prop.getProperty("sleepTime");
			targeturl = prop.getProperty("targeturl");
			String iFileSize = prop.getProperty("ignoreFileSize") ;
			ignoreFileSize = Integer.parseInt(iFileSize);

		} catch (Throwable ex) {
			System.err.println(ex);
		}
	}

	public static double printFileSize(String fileLocation) {
		File file =new File(fileLocation);
		double kilobytes = 0;
		if(file.exists()){

			double bytes = file.length();
			kilobytes = (bytes / 1024);
			System.out.println("File size before upload "+ df.format(kilobytes) + "KB");
		}
		return kilobytes;
	}

	public static void readFile(String filePath)
			throws IOException {

		// printFileContents(filePath);
		Path path = Paths.get(filePath);

		AsynchronousFileChannel channel = AsynchronousFileChannel.open(path);
		long position = 0;
		ByteBuffer buffer = ByteBuffer.allocate(1024);

		channel.read(buffer, position, buffer, new CompletionHandler<Integer, ByteBuffer>() {
			@Override
			public void completed(Integer result, ByteBuffer attachment) {
				System.out.println("result = " + result);

				attachment.flip();
				byte[] data = new byte[attachment.limit()];
				attachment.get(data);
				System.out.println(new String(data));
				attachment.clear();
				Boolean res =invokeUploadFile(filePath);
				if(res) {
					deleteFile(filePath);
				}else {
					System.out.println("upload failed for the file "+ filePath);
				}
			}

			@Override
			public void failed(Throwable exc, ByteBuffer attachment) {
				System.out.println(attachment + " failed with exception:");
			}
		});

		//        channel.read(buffer, 0, "Read operation ALFA",
		//            new CompletionHandler<Integer, A>() {
		//			};() {
		//                @Override
		//                public void completed(Integer result, Object attachment) { 
		//                    System.out.println(attachment + " completed and " + result + " bytes are read. ");
		//                    currentThread.interrupt();
		//                } 
		//                @Override
		//                public void failed(Throwable e, Object attachment) {
		//                    System.out.println(attachment + " failed with exception:");
		//                    e.printStackTrace();
		//                    currentThread.interrupt();
		//                }
		//        });

		System.out.println ("Waiting for completion...");



		buffer.flip();

		System.out.print("Buffer contents: ");

		while (buffer.hasRemaining()) {

			System.out.print((char) buffer.get());                
		}
		System.out.println(" ");

		buffer.clear();
		channel.close();
	}

	public static void printFileContents(String path)
			throws IOException {

		FileReader fr = new FileReader(path);
		BufferedReader br = new BufferedReader(fr);

		String textRead = br.readLine();

		while (textRead != null) {

			textRead = br.readLine();
		}

		fr.close();
		br.close();
	}

	public static void sendResponse(String json) {
		try {
			URL myurl = new URL(targeturl);
			HttpURLConnection con = (HttpURLConnection)myurl.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);

			con.setRequestProperty("Content-Type", "application/json;");
			con.setRequestProperty("Accept", "application/json,text/plain");
			con.setRequestProperty("Method", "POST");
			OutputStream os = con.getOutputStream();
			os.write(json.toString().getBytes("UTF-8"));
			os.close();


			StringBuilder sb = new StringBuilder();  
			int HttpResult =con.getResponseCode();
			if(HttpResult ==HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new   InputStreamReader(con.getInputStream(),"utf-8"));  

				String line = null;
				while ((line = br.readLine()) != null) {  
					sb.append(line + "\n");  
				}
				br.close(); 
				System.out.println(""+sb.toString());  

			}else{
				System.out.println(con.getResponseCode());
			}  
		}catch(Throwable e){
			System.err.println("Exception while invoking webhook URL "+ e);
		}

	}

}
